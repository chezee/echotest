//
//  ETData.h
//  ECHOtest
//
//  Created by Илья Пупкин on 10/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ETDataType)
{
    ETDataTypeXML = 0,
    ETDataTypeJSON,
    ETDataTypeBinary
};

@interface ETData : NSObject

@property (nonatomic, strong) NSDictionary *data;

- (void) save:(NSDictionary *)data;
- (NSManagedObjectContext *)managedObjectContext;

@end
