//
//  ETData.m
//  ECHOtest
//
//  Created by Илья Пупкин on 10/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ETData.h"
#import "ETViewController.h"
#import "ETSocket.h"

@interface ETData ()

@property (nonatomic, strong) NSManagedObjectContext *context;
@property NSUInteger count;

@end

@implementation ETData

@synthesize context = _context;
@synthesize data = _data;


- (id) init
{
    if(self = [super init])
    {
        [[ETSocket shared] setupObserverFor:self];
        
        self.context = [self managedObjectContext];
        
        NSError *error = nil;
        if(![self.context save:&error])
            NSLog(@"Cant save with error: %@, %@", error, [error localizedDescription]);
        NSMutableArray *posts = [[NSMutableArray alloc]init];
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Post"];
        posts = [[self.context executeFetchRequest:fetchRequest error:nil] mutableCopy];
        NSLog(@"%@", posts);
    }
    
    return self;
}

- (void) save:(NSDictionary *)data
{
    NSManagedObject *newPost = [NSEntityDescription insertNewObjectForEntityForName:@"Post"
                                                             inManagedObjectContext:self.context];
    
    self.data = data;
    
    [newPost setValue:[self.data objectForKey:@"uID"]           forKey:@"uID"];
    [newPost setValue:[self.data objectForKey:@"string"]        forKey:@"stringData"];
    [newPost setValue:[self.data objectForKey:@"bool"]          forKey:@"boolData"];
    [newPost setValue:[self.data objectForKey:@"date"]          forKey:@"dateData"];
    [newPost setValue:[self.data objectForKey:@"delivered"]     forKey:@"delivered"];
    
    NSError *error = nil;
    
    if (![self.context save:&error])
    {
        NSLog(@"%@", error);
    }
    
    NSLog(@"%@", newPost);
    
}

- (NSManagedObjectContext *)managedObjectContext
{
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(managedObjectContext)])
        self.context = [delegate managedObjectContext];
    return self.context;
}

-(void)dealloc
{
    [self removeObserver:[ETSocket shared] forKeyPath:@"data"];
    [super dealloc];
}

@end
