//
//  ETSocket.h
//  ECHOtest
//
//  Created by Илья Пупкин on 10/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIkit.h>
#import <Foundation/Foundation.h>
#import <KGWebSocket/WebSocket.h>

@interface ETSocket : NSObject

+ (ETSocket *) shared;
- (BOOL) connected;
- (void) connect;
- (void) disconnect;

- (void) setupObserverFor:(id)object;

@end
