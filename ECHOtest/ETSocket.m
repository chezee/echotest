//
//  ETSocket.m
//  ECHOtest
//
//  Created by Илья Пупкин on 10/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "ETSocket.h"
#import "ETData.h"

static NSString * const ETLocation = @"wss://sandbox.kaazing.net/echo";


@interface ETSocket ()

@property (nonatomic, retain) KGWebSocket *socket;
@property (nonatomic, retain) KGWebSocketFactory *factory;

@end

@implementation ETSocket

+ (ETSocket *) shared
{
    static dispatch_once_t predicate = 0;
    __strong static ETSocket *sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [[[self alloc] init] retain];
    });
    return sharedObject;
}

- (BOOL)connected
{
    return self.socket.readyState == KGReadyState_OPEN;
}

- (void)connect
{
    if ([self connected]) return;
    
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [self.factory release];
        [self.socket release];
        
        self.factory = [[KGWebSocketFactory createWebSocketFactory] retain];
        self.socket = [[self.factory createWebSocket:[NSURL URLWithString:ETLocation]] retain];
        
        //KGChallengeHandler *challengeHandler = [[ETSocket sharedManager] createBasicChallengeHandler];
        //[webSocketFactory setDefaultChallengeHandler:challengeHandler];
        
        [self setupListeners];
        [self.socket connect];
    });
}

- (void)disconnect
{
    if (![self connected]) return;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.socket close];
    });
}

- (void) setupListeners
{
    self.socket.didOpen = ^(KGWebSocket* webSocket)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ETSocketStateChanged"
                                                            object:nil];
    };

    self.socket.didReceiveMessage = ^(KGWebSocket* webSocket, id data)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ETSocketReceivedMessage"
                                                            object:nil
                                                          userInfo:data];
    };

    self.socket.didReceiveError = ^(KGWebSocket* webSocket, NSError *error)
    {
        NSLog(@"Recieved error: %@", error);
    };

    self.socket.didClose = ^(KGWebSocket* websocket, NSInteger code, NSString* reason, BOOL wasClean)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ETSocketStateChanged"
                                                            object:nil];
    };
}

- (void) send:(NSDictionary *)data
{
    NSError *error;
    NSDate *date = [data objectForKey:@"date"];
    ETDataType format = [[data objectForKey:@"format"] integerValue];
    
    if (format == ETDataTypeJSON)
    {
        NSDictionary *dict = @{@"id": [data objectForKey:@"uID"],
                               @"string" : [data objectForKey:@"string"],
                               @"date": @([date timeIntervalSince1970]),
                               @"bool": [data objectForKey:@"bool"]};
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        NSString *payload = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [self.socket send:payload];
    }
    else if (format == ETDataTypeXML)
    {
        NSString *payload = [NSString stringWithFormat:
                             @"<data>\n   <id>%@</id>\n   <string>%@</string>\n   <date>%.0f</date>\n   <bool>%i</bool>\n<data>",
                             [data objectForKey:@"uID"],
                             [data objectForKey:@"string"],
                             [date timeIntervalSince1970],
                             [[data objectForKey:@"bool"] intValue]];
        
        [self.socket send:payload];
    }
    else
    {
        NSData *d = [NSKeyedArchiver archivedDataWithRootObject:@{@"id": [data objectForKey:@"uID"],
                                                                  @"string" : [data objectForKey:@"string"],
                                                                  @"date": [data objectForKey:@"date"],
                                                                  @"bool": [data objectForKey:@"bool"]}];
        [self.socket send:d];
    }
}

- (void) setupObserverFor:(id)object
{
    [object addObserver:self
             forKeyPath:@"data"
                options:NSKeyValueObservingOptionNew
                context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    NSString *key = @"data";
    
    if ([keyPath isEqual:key])
    {
        [object removeObserver:self forKeyPath:key];
        [self send:[change objectForKey:@"new"]];
    }
}

@end

