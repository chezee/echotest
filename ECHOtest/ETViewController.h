//
//  ViewController.h
//  ECHOtest
//
//  Created by Илья Пупкин on 10/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "ETData.h"
#import "ETSocket.h"
#import <UIKit/UIKit.h>

@interface ETViewController : UIViewController <UITableViewDelegate,
                                                UITableViewDataSource,
                                                NSXMLParserDelegate>

@property (strong, nonatomic) IBOutlet UITextView *logText;
@property (strong, nonatomic) IBOutlet UITextField *inputText;
@property (strong, nonatomic) IBOutlet UISegmentedControl *stateSendControll;
@property (strong, nonatomic) IBOutlet UISwitch *boolSwitcher;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, atomic) NSMutableArray *receivedPosts;
@property (atomic,assign) BOOL *messageDelieveryState;


@property (nonatomic,strong) NSMutableArray *arrXMLData;
@property (nonatomic,strong) NSMutableString *strXMLString;
@property (nonatomic,strong) NSMutableDictionary *dictXMLPart;


@property (nonatomic, strong) IBOutlet UIView *topView;
@property (nonatomic, strong) IBOutlet UIView *bottomView;

@property (nonatomic, strong) IBOutlet UITableView *tableLeft;
@property (nonatomic, strong) IBOutlet UITableView *tableRight;

- (IBAction)clearCoreData:(id)sender;
- (IBAction)sendMessage:(id)sender;
- (IBAction)setStateSendControll:(UISegmentedControl *)stateSendControll;
- (IBAction)connectButton:(id)sender;
- (IBAction)disconnect:(id)sender;

@end

