//
//  ViewController.m
//  ECHOtest
//
//  Created by Илья Пупкин on 10/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "ETViewController.h"
#import "ETData.h"
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import "xmlreader.h"

static NSString * const leftCellIdentifier = @"leftCell";
static NSString * const rightCellIdentifier = @"rightCell";

@implementation ETViewController
{
    UIRefreshControl *refresh;
    NSMutableArray *posts;
    
}

@synthesize tableRight;
@synthesize tableLeft;
@synthesize logText;
@synthesize inputText;
@synthesize sendButton;
@synthesize boolSwitcher;
@synthesize stateSendControll;
@synthesize messageDelieveryState;
@synthesize receivedPosts;
@synthesize arrXMLData;
@synthesize dictXMLPart;
@synthesize strXMLString;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.receivedPosts = [NSMutableArray array];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(webSocketDidChange:)
                                                 name:@"ETSocketStateChanged"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(webSocketReceivedMessage:)
                                                 name:@"ETSocketReceivedMessage"
                                               object:nil];
    
    [self reassingPosts];
    [self log:[NSString stringWithFormat:@"Fetching from CoreData"] andTextView:logText];
}

- (void) reassingPosts
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Post"];
    [posts removeAllObjects];
    posts = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
}

#pragma mark UI

- (IBAction)clearCoreData:(id)sender {
    [self deleteAllObjectsWithEntityNameinContext:[self managedObjectContext]];
    [self reassingPosts];
    [tableLeft reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableLeft){
        UITableViewCell *leftCell = [tableView dequeueReusableCellWithIdentifier:leftCellIdentifier
                                                                    forIndexPath:indexPath];
        
        leftCell.textLabel.text = [[posts objectAtIndex:indexPath.row]
                                            valueForKey:@"uID"];
        
        for(NSDictionary *post in receivedPosts)
        {
            if ([[post objectForKey:@"id"] isEqual:[[posts objectAtIndex:indexPath.row] valueForKey:@"uID"]])
                leftCell.backgroundColor = [UIColor greenColor];
        }
        leftCell.detailTextLabel.text = [NSString stringWithFormat:@"sent: %@", [NSDate date]];
        return leftCell;
    }
    else {
        UITableViewCell *rightCell = [tableView dequeueReusableCellWithIdentifier:rightCellIdentifier
                                                                     forIndexPath:indexPath];
        rightCell.textLabel.text = [[receivedPosts objectAtIndex:indexPath.row] valueForKey:@"string"];
        rightCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [[receivedPosts objectAtIndex:indexPath.row] valueForKey:@"date"]];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(75, 10, 150, 15)];
        label.font = [UIFont systemFontOfSize:14.0];
        label.text = [NSString stringWithFormat:@"Bool data: %@", [[receivedPosts objectAtIndex:indexPath.row] valueForKey:@"bool"]];
        [rightCell.contentView addSubview:label];
        return rightCell;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableLeft) {
        return [posts count];
    }
    else
        return [receivedPosts count];
}

#pragma mark coreData

- (IBAction)sendMessage:(id)sender
{
    ETData *entityToSend = [[[ETData alloc] init] retain];
    NSString *uID = [NSString stringWithFormat:@"%i%.0f%i",
                     arc4random_uniform(9999), [[NSDate date] timeIntervalSince1970], arc4random_uniform(9999)];
    
    
    NSDictionary *data = @{@"uID": uID,
                           @"string" : self.inputText.text,
                           @"date": [NSDate date],
                           @"bool": @(boolSwitcher.isOn),
                           @"format": @(stateSendControll.selectedSegmentIndex),
                           @"delivered": @(NO)};
    [entityToSend save:data];
    [self log:[NSString stringWithFormat:@"Saving to Core data: %@", data] andTextView:logText];
    [self reassingPosts];
    [tableLeft reloadData];
}

- (void)log:(NSString *)msg andTextView:(UITextView *) textView {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *text = msg;
        NSString *log = [textView text];
        
        if ((log != nil) && ([log length] > 0)) {
            text = [NSString stringWithFormat:@"%@\n%@", [textView text], msg];
        }
        
        if ([[textView text] length] > 5000) {
            text = [text substringFromIndex:3000];
        }
        [textView setText:text];
        [textView scrollRangeToVisible:NSMakeRange([textView.text length], 0)];
    });
}

- (NSManagedObjectContext *) managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
        context = [delegate managedObjectContext];
    return context;
}

- (void)deleteAllObjectsWithEntityNameinContext:(NSManagedObjectContext *)context
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Post"];
        fetchRequest.includesPropertyValues = NO;
        fetchRequest.includesSubentities = NO;
        
        NSError *error;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        
        for (NSManagedObject *managedObject in items) {
            [context deleteObject:managedObject];
        }
        [posts removeAllObjects];
        [tableLeft reloadData];
    });
    [self log:@"Deleting all entities in coreData" andTextView:logText];
}



#pragma mark Connect/disconnect

- (IBAction)connectButton:(id)sender
{
    [[ETSocket shared] connect];
    [self log:@"Connecting to server" andTextView:logText];
}

- (IBAction)disconnect:(id)sender
{
    @try
    {
       [[ETSocket shared] disconnect];
    }
    @catch (NSException *exception)
    {
        [self log:[NSString stringWithFormat:@"Disconnecting because of exception: %@", [exception reason]]andTextView:logText];
    }
    [self log:@"Disconnecting" andTextView:logText];
}

#pragma mark Notifications

- (void) webSocketReceivedMessage:(NSNotification *)notification
{
    NSError *error;
    NSData *dataForParse = [[NSString stringWithFormat:@"%@", notification.userInfo] dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:dataForParse options:kNilOptions
                                                error:&error];
    if(json)
        [self postReceived:json];
    else {
        NSDictionary *xmlDict = [NSDictionary dictionaryWithXMLData:dataForParse];
        if (!xmlDict){
            NSDictionary *binaryData = [NSKeyedUnarchiver unarchiveObjectWithData:notification.userInfo];
            [self postReceived:binaryData];
        }
        else
            [self postReceived:xmlDict];
    }
}

- (void) postReceived:(NSDictionary *)post
{
    [self.receivedPosts addObject:post];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [tableRight reloadData];
        [tableLeft reloadData];
    });
}

- (void) webSocketDidChange:(NSNotification *)notification
{
    UIColor *color = [[ETSocket shared] connected] ?
    [[UIColor greenColor] colorWithAlphaComponent:0.5] :
    [UIColor whiteColor];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.topView.backgroundColor = color;
    });
    [self log:[NSString stringWithFormat:@"Connection status: %d", [[ETSocket shared]connected]] andTextView:logText];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

@end
